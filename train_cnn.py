import multiprocessing as mp
from queue import Empty, Full
from typing import List, Tuple

import torch
from torch.utils.data.dataloader import default_collate

from detection.frcnn.frcnn_model import save_model
from detection.mnist.CNN import CNN
from util.dataset_util import get_dc_dataset

END_CODE = 0
CHILDREN_COUNT = 4
EPOCH_COUNT = 100
EPOCH_SIZE = 1000
QUEUE_SIZE = 10000
BATCH_SIZE = 128


class ChildProcess(object):

    def __init__(self, index: int, process: mp.Process, input_queue: mp.Queue):
        self.index = index
        self.process = process
        self.input_queue = input_queue


def log(message: str):
    pass


def should_exit(input_queue: mp.Queue):
    try:
        value = input_queue.get(block=False)
        return value == END_CODE
    except Empty:
        return False


def try_add(index: int, input_queue: mp.Queue, output_queue: mp.Queue, batch):
    log("Child {} trying to add batch".format(index))
    while not should_exit(input_queue):
        try:
            output_queue.put(batch)
            log("Child {} added batch".format(index))
            return
        except Full:
            log("Child {} failed at adding batch".format(index))


def child_start(index: int, input_queue: mp.Queue, output_queue: mp.Queue, batch_size=128):
    log("Child {} starting".format(index))
    dataset = get_dc_dataset()

    while not should_exit(input_queue):
        batch = []
        current_batch_size = 0

        while current_batch_size < batch_size:
            mini_batch = dataset[0]
            mini_batch_length = len(mini_batch[0])
            empty_slots = batch_size - current_batch_size
            to_add = mini_batch_length if empty_slots >= mini_batch_length else empty_slots

            batch.extend(mini_batch[0:to_add])
            current_batch_size += to_add

        batch = default_collate(batch)
        try_add(index, input_queue, output_queue, batch)

    log("Child {} exiting".format(index))


def create_children(children_count: int, output_queue_size: int, batch_size: int) -> \
        Tuple[mp.Queue, List[ChildProcess]]:
    child_processes = []
    _output_queue = mp.Queue(maxsize=output_queue_size)

    for child_index in range(children_count):
        input_queue = mp.Queue()
        child = mp.Process(target=child_start, args=(child_index, input_queue, _output_queue, batch_size),
                           daemon=True)
        child_processes.append(ChildProcess(child_index, child, input_queue))

    return _output_queue, child_processes


def start_children(children: List[ChildProcess]):
    for child in children:
        child.process.start()


def destroy_children(children: List[ChildProcess]):
    for child in children:
        child.input_queue.put(END_CODE)
    for child in children:
        child.process.join()


def train(input_queue: mp.Queue):
    device = ('cuda' if torch.cuda.is_available() else 'cpu')

    model = CNN()
    model.to(device)

    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=0.01, momentum=0.9, weight_decay=0.0005)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.5)
    criterion = torch.nn.CrossEntropyLoss()

    for epoch in range(EPOCH_COUNT):
        # prep model for training
        model.train()
        train_loss = 0

        for idx in range(EPOCH_SIZE):
            # Send these >>> To GPU
            images, labels = input_queue.get(block=True)
            images = images.to(device)
            labels = labels.to(device)

            # Training pass
            optimizer.zero_grad()

            # Forward pass
            output = model(images)
            loss = criterion(output, labels)

            # Backward pass
            loss.backward()
            optimizer.step()

            iter_loss = loss.item()
            train_loss += iter_loss
            print('epoch: {}/{}, iter: {}/{} loss: {}'.format(epoch + 1, EPOCH_COUNT, idx + 1, EPOCH_SIZE, iter_loss))

        else:
            # prep model for evaluation
            model.eval()
            test_loss = 0
            accuracy = 0

            # Turn off the gradients when performing validation.
            # If we don't turn it off, we will comprise our networks weight entirely
            with torch.no_grad():
                images, labels = input_queue.get()
                images = images.to(device)
                labels = labels.to(device)

                log_probabilities = model(images)
                test_loss += criterion(log_probabilities, labels)

                probabilities = torch.exp(log_probabilities)
                top_prob, top_class = probabilities.topk(1, dim=1)
                predictions = top_class == labels.view(*top_class.shape)
                accuracy += torch.mean(predictions.type(torch.FloatTensor))

            print("Epoch: {}/{}  ".format(epoch + 1, EPOCH_COUNT),
                  "Testing loss: {:.4f}  ".format(test_loss),
                  "Trainging loss: {:.4f}  ".format(train_loss / EPOCH_SIZE),
                  "Test accuracy: {:.4f}  ".format(accuracy))

        if (epoch + 1) % 5 == 0:
            save_model(model, 'weights/dc/greyscale_dc_2_{}.weights'.format(epoch))

        lr_scheduler.step()

    save_model(model, 'weights/dc/greyscale_dc_2_final.weights')


if __name__ == '__main__':
    output_queue, children = create_children(children_count=CHILDREN_COUNT, output_queue_size=QUEUE_SIZE,
                                             batch_size=BATCH_SIZE)
    start_children(children)
    train(output_queue)
    destroy_children(children)

from typing import Tuple

from result_sheet.registry.dto import ResultSheetElement, ResultSheetElementType
from result_sheet.registry.interfaces import ResultSheetElementRegistry


class DefaultResultSheetElementRegistry(ResultSheetElementRegistry):

    def __init__(self):
        checkboxes = [
            ResultSheetElement((299, 197), (14, 14), ResultSheetElementType.CHECKBOX),
            ResultSheetElement((299, 228), (14, 14), ResultSheetElementType.CHECKBOX),
            ResultSheetElement((299, 259), (14, 14), ResultSheetElementType.CHECKBOX),
        ]

        signatures = [
            ResultSheetElement((581, 622), (140, 36), ResultSheetElementType.SIGNATURE),
            ResultSheetElement((554, 1105), (140, 36), ResultSheetElementType.SIGNATURE),
        ]

        credentials_table_info = [
            ResultSheetElement((219, 336), (335, 36), ResultSheetElementType.CREDENTIALS_TABLE_INFO),
            ResultSheetElement((221, 410), (498, 24), ResultSheetElementType.CREDENTIALS_TABLE_INFO),
            ResultSheetElement((221, 434), (498, 24), ResultSheetElementType.CREDENTIALS_TABLE_INFO),
            ResultSheetElement((221, 458), (165, 24), ResultSheetElementType.CREDENTIALS_TABLE_INFO),
            ResultSheetElement((553, 458), (165, 24), ResultSheetElementType.CREDENTIALS_TABLE_INFO),
        ]
        full_credentials_table_info = [
            ResultSheetElement((219, 336), (335, 36), ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO),
            ResultSheetElement((56, 410), (664, 24), ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO),
            ResultSheetElement((56, 434), (664, 24), ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO),
            ResultSheetElement((56, 458), (330, 24), ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO),
            ResultSheetElement((338, 458), (330, 24), ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO),
        ]

        offsets = [0, 25, 24, 25, 24, 24, 25, 24, 25, 24]

        result_rows = [ResultSheetElement((223, 803 + sum(offsets[:i + 1])), (140, 24),
                                          ResultSheetElementType.RESULT_ROW) for i in range(10)] + \
                      [ResultSheetElement((553, 803 + sum(offsets[:i + 1])), (140, 24),
                                          ResultSheetElementType.RESULT_ROW) for i in range(10)]
        full_result_rows = [ResultSheetElement((85, 803 + sum(offsets[:i + 1])), (280, 24),
                                               ResultSheetElementType.FULL_RESULT_ROW) for i in range(10)] + \
                           [ResultSheetElement((413, 803 + sum(offsets[:i + 1])), (280, 24),
                                               ResultSheetElementType.FULL_RESULT_ROW) for i in range(10)]

        final_result = [
            ResultSheetElement((556, 1073), (138, 24), ResultSheetElementType.FINAL_RESULT_ROW),
        ]

        tables = [
            ResultSheetElement((56, 407), (664, 80), ResultSheetElementType.TABLE),
            ResultSheetElement((85, 776), (280, 273), ResultSheetElementType.TABLE),
            ResultSheetElement((413, 776), (280, 273), ResultSheetElementType.TABLE),
            ResultSheetElement((416, 1070), (277, 28), ResultSheetElementType.TABLE),
        ]

        self.type_map = {
            ResultSheetElementType.CHECKBOX: checkboxes,
            ResultSheetElementType.SIGNATURE: signatures,
            ResultSheetElementType.CREDENTIALS_TABLE_INFO: credentials_table_info,
            ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO: full_credentials_table_info,
            ResultSheetElementType.RESULT_ROW: result_rows,
            ResultSheetElementType.FULL_RESULT_ROW: full_result_rows,
            ResultSheetElementType.FINAL_RESULT_ROW: final_result,
            ResultSheetElementType.TABLE: tables,
        }

    def __getitem__(self, item: Tuple[ResultSheetElementType, int]) -> ResultSheetElement:
        return self.type_map[item[0]][item[1]]

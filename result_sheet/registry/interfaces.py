from typing import Dict, Tuple

from result_sheet.character.dto import CharacterImage
from result_sheet.registry.dto import ResultSheetElement, ResultSheetElementType, ResultSheetImage, ResultSheetImageType


class CharacterImageRegistry(object):

    def __getitem__(self, label: str) -> CharacterImage:
        pass


class ResultSheetElementRegistry(object):

    def __getitem__(self, item: Tuple[ResultSheetElementType, int]) -> ResultSheetElement:
        pass


class ResultSheetImageRegistry(object):

    def get_rs_image(self, _type: ResultSheetImageType, offset: Tuple[int, int] = (0, 0), index: int = None,
                     additional_attributes: Dict[object, object] = None) -> ResultSheetImage:
        pass

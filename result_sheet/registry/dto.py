from enum import IntEnum
from typing import Dict, Tuple

from PIL.Image import Image

from util.tuple_util import tuple_add


class ResultSheetElementType(IntEnum):
    CHECKBOX = 0
    SIGNATURE = 1
    CREDENTIALS_TABLE_INFO = 2
    FULL_CREDENTIALS_TABLE_INFO = 3
    RESULT_ROW = 4
    FULL_RESULT_ROW = 5
    FINAL_RESULT_ROW = 6
    TABLE = 7


class ResultSheetImageType(IntEnum):
    RESULT_SHEET = 0
    CREDENTIALS_TABLE = 1
    FIRST_RESULT_TABLE = 2
    SECOND_RESULT_TABLE = 3
    FINAL_RESULT_TABLE = 4
    JMBAG_ROW = 5
    RESULT_ROW = 6
    FINAL_RESULT_ROW = 7


class ResultSheetElement(object):

    def __init__(self, start: Tuple[int, int], size: Tuple[int, int], _type: ResultSheetElementType):
        self.start = start
        self.size = size
        self.type = _type

    @property
    def end(self):
        return tuple_add(self.start, self.size)

    @property
    def bounding_box(self):
        return self.start, self.end


class ResultSheetImage(object):

    def __init__(self, image: Image, _type: ResultSheetImageType, index: int = None, offset: Tuple[int, int] = (0, 0),
                 additional_attributes: Dict[object, object] = None):
        if additional_attributes is None:
            additional_attributes = {}

        self.image = image
        self.type = _type
        self.index = index
        self.offset = offset
        self.additional_attributes = additional_attributes

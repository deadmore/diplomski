from typing import Dict, Tuple

from PIL import Image

from result_sheet.registry.dto import ResultSheetElementType, ResultSheetImage, ResultSheetImageType
from result_sheet.registry.interfaces import ResultSheetElementRegistry, ResultSheetImageRegistry
from util.image_util import crop


class DefaultResultSheetImageRegistry(ResultSheetImageRegistry):

    def __init__(self, path: str, rs_element_registry: ResultSheetElementRegistry):
        self.result_sheet = Image.open(path).convert('L')
        self.bb_boxes = {}
        types = [ResultSheetImageType.CREDENTIALS_TABLE, ResultSheetImageType.FIRST_RESULT_TABLE,
                 ResultSheetImageType.SECOND_RESULT_TABLE, ResultSheetImageType.FINAL_RESULT_TABLE]

        for index, table in enumerate(types):
            rs_table = rs_element_registry[ResultSheetElementType.TABLE, index]
            self.bb_boxes[table] = rs_table.bounding_box

        full_jmbag_row = rs_element_registry[ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO, 2]
        self.bb_boxes[ResultSheetImageType.JMBAG_ROW] = full_jmbag_row.bounding_box

        final_result_row = rs_element_registry[ResultSheetElementType.TABLE, 3]
        self.bb_boxes[ResultSheetImageType.FINAL_RESULT_ROW] = final_result_row.bounding_box

        self.bb_boxes[ResultSheetImageType.RESULT_ROW] = []
        for index in range(20):
            rs_result_row = rs_element_registry[ResultSheetElementType.FULL_RESULT_ROW, index]
            self.bb_boxes[ResultSheetImageType.RESULT_ROW].append(rs_result_row.bounding_box)

    def get_rs_image(self, _type: ResultSheetImageType, offset: Tuple[int, int] = (0, 0), index: int = None,
                     additional_attributes: Dict[object, object] = None) -> ResultSheetImage:
        if _type == ResultSheetImageType.RESULT_SHEET:
            return ResultSheetImage(self.result_sheet.copy(), ResultSheetImageType.RESULT_SHEET,
                                    additional_attributes=additional_attributes)
        elif _type == ResultSheetImageType.RESULT_ROW:
            bb_box = self.bb_boxes[ResultSheetImageType.RESULT_ROW][index]
        else:
            bb_box = self.bb_boxes[_type]

        image, start = crop(self.result_sheet, bb_box, offset)

        return ResultSheetImage(image, _type, index=index, offset=start, additional_attributes=additional_attributes)

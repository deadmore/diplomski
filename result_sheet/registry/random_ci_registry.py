from typing import Union

from result_sheet.character.dto import CharacterImage
from result_sheet.character.interfaces import CharacterImageLoader
from result_sheet.crandom.funcs import random_int
from result_sheet.registry.interfaces import CharacterImageRegistry


class RandomCharacterImageRegistry(CharacterImageRegistry):

    def __init__(self, loader: CharacterImageLoader):
        self.registry = {}

        characters = loader.load()
        for key, value in characters.items():
            self.registry[key] = value

    def __getitem__(self, label: Union[chr, int]) -> CharacterImage:
        characters = self.registry[label]
        _max = len(characters)
        index = random_int(_max - 1)

        return characters[index]

from random import choice as _choice, randint, random
from typing import List, TypeVar

X = TypeVar('X')


def random_int(max: int, min: int = 0) -> int:
    return randint(min, max)


def random_int_sequence(length: int, min: int = 0, max: int = 9) -> List[int]:
    return [random_int(max=max, min=min) for x in range(length)]


def choice(values: List[X]) -> X:
    return _choice(values)


def toss_coin() -> bool:
    return random() < 0.5

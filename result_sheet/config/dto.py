from enum import IntEnum
from typing import List


class TestType(IntEnum):
    MID = 0
    FINAL = 1
    TERM = 2


class Test(object):

    def __init__(self, subject: str, date: str, test_type: TestType, reviewer_name):
        self.subject = subject
        self.date = date
        self.test_type = test_type
        self.reviewer_name = reviewer_name


class Student(object):

    def __init__(self, name: str, jmbag: str, group: str):
        self.name = name
        self.jmbag = jmbag
        self.group = group


class Result(object):

    def __init__(self, first: str, second: str = None):
        self.first = first
        self.second = second


class WritingStyle(object):

    def __init__(self, space_size: int, distance: int, size: int):
        self.space_size = space_size
        self.distance = distance
        self.size = size


class ResultSheetConfig(object):

    def __init__(self, student: Student, test: Test, results: List[Result], writing_style: WritingStyle):
        self.student = student
        self.test = test
        self.results = results
        self.writing_style = writing_style

    @property
    def final_result(self):
        return str(sum(map(lambda x: int(x.first), self.results)))

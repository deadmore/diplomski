FIRST_NAMES = ['ante', 'anotnio', 'antonijo', 'andrej', 'ardian', 'adrian', 'alen', 'andjelko',
               'matej', 'marko', 'matija', 'marin', 'slaven', 'luka', 'nikola', 'petar', 'ivan', 'josip',
               'mislav', 'tomislav', 'hrvoje', 'otto', 'sanjin', 'kristijan', 'stjepan', 'bernard',
               'dominik', 'david', 'vladimir', 'damir', 'marijan', 'kreso', 'vjeko', 'sasa',
               'andrija', 'boris', 'jakov', 'josip', 'pavao', 'pavle', 'simun', 'gabrijel', 'kresimir',
               'rafael', 'dante', 'aleksandar', 'igor', 'mario', 'zvonimir', 'stansko', 'zvonko', 'nikolina',
               'ana', 'matea', 'vinka', 'lora', 'visnja', 'zorica', 'petra', 'ivana', 'veronika',
               'andrea', 'gabrijela', 'hana', 'elena', 'josipa', 'marija', 'anka', 'manda',
               'franka', 'franko', 'kristina', 'dubravka', 'jadranka', 'ivona', 'helena']

LAST_NAMES = ['curic', 'sanader', 'mamic', 'subasic', 'modric', 'engelmann', 'corluka', 'drmic',
              'lukic', 'bradaric', 'rados', 'perko', 'pantina', 'gasparini', 'domazet',
              'misevic', 'basic', 'kukic', 'bunarkic', 'muller', 'sola', 'smoljo', 'raguz',
              'vrsaljko', 'car', 'markovic', 'antunovic', 'doric', 'rajic', 'zeljeznik',
              'dzakula', 'markulincic', 'cupic', 'fertelja', 'krois', 'andric', 'jaman',
              'bacmaga', 'silic', 'milisavljevic', 'gudelj', 'klepo', 'bartolac', 'marenci',
              'kostelac', 'jadric', 'krusec', 'doko', 'car', 'kralj', 'beslic', 'mestrovic',
              'jelacic', 'starcevic', 'radic', 'zeravica', 'plazonja', 'petrovic', 'spajic',
              'marijanovic', 'knezovic', 'martinovic', 'begonja', 'oreskovic']

SUBJECTS = ['sis', 'tinf', 'mat', 'os', 'opp', 'ppp', 'oop', 'optjava', 'ele', 'vjekom', 'fizika',
            'oe', 'pipi', 'asp', 'arh', 'utr', 'ppj', 'irg']

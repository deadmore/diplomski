from result_sheet.config.dto import *
from result_sheet.config.random_rs_config_generator import RandomResultSheetConfigGenerator
from result_sheet.crandom.funcs import random_int

MAX_ROW_COUNT = 12


class RealResultSheetConfigGenerator(RandomResultSheetConfigGenerator):

    def _generate_results(self) -> List[Result]:
        row_count = random_int(MAX_ROW_COUNT)
        return [self._generate_result() for x in range(row_count)]

from result_sheet.config.data import *
from result_sheet.config.dto import *
from result_sheet.config.interfaces import ResultSheetConfigGenerator
from result_sheet.crandom.funcs import choice, random_int, random_int_sequence, toss_coin

_max_value = 99
_max_dec_value = 99


# noinspection PyMethodMayBeStatic
class RandomResultSheetConfigGenerator(ResultSheetConfigGenerator):

    def _generate_student(self) -> Student:
        first_name_index = random_int(len(FIRST_NAMES) - 1)
        last_name_index = random_int(len(LAST_NAMES) - 1)
        jmbag = "".join(list(map(lambda x: str(x), random_int_sequence(10))))
        group = "p0{}".format(random_int(20))

        name = FIRST_NAMES[first_name_index] + " " + LAST_NAMES[last_name_index]

        return Student(name, jmbag, group)

    def _generate_test(self) -> Test:
        subject_index = random_int(len(SUBJECTS) - 1)
        first_name_index = random_int(len(FIRST_NAMES) - 1)
        last_name_index = random_int(len(LAST_NAMES) - 1)

        subject = SUBJECTS[subject_index]

        day = random_int(31)
        month = random_int(12)
        year = random_int(9999, 1000)
        date = "{} {} {}".format(day, month, year)

        reviewer_name = FIRST_NAMES[first_name_index] + " " + LAST_NAMES[last_name_index]

        test_type = choice([TestType.MID, TestType.FINAL, TestType.TERM])

        return Test(subject, date, test_type, reviewer_name)

    def _generate_result(self) -> Result:
        first_result = str(random_int(_max_value))
        second_result = None

        if toss_coin():
            second_result = str(random_int(_max_value))

        return Result(first_result, second_result)

    def _generate_results(self) -> List[Result]:
        return [self._generate_result() for x in range(20)]

    def _generate_writing_style(self) -> WritingStyle:
        space_size = random_int(5)
        distance = random_int(max=20, min=2)
        size = random_int(5)

        return WritingStyle(space_size, distance, size)

    def generate(self) -> ResultSheetConfig:
        user = self._generate_student()
        test = self._generate_test()
        results = self._generate_results()
        writing_style = self._generate_writing_style()

        return ResultSheetConfig(user, test, results, writing_style)

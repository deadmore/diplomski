from typing import List

from result_sheet.config.dto import WritingStyle
from result_sheet.crandom.funcs import random_int
from result_sheet.generator.dto import PasteObject, ResultSheet, ResultSheetObject
from result_sheet.generator.interfaces import HAlignment, Paster, VAlignment
from result_sheet.registry.dto import ResultSheetElement
from util.tuple_util import tuple_sub


class DefaultPaster(Paster):

    @staticmethod
    def _get_expression_width(characters: List[PasteObject], writing_style: WritingStyle) -> int:
        x = 0
        length = 0

        for character in characters:
            if character is not None:
                x += character.size[0]
                length += 1
            else:
                x += writing_style.space_size

        return x + (length - 1) * writing_style.distance

    def paste(self, result_sheet: ResultSheet, result_sheet_element: ResultSheetElement, objects: List[PasteObject],
              writing_style: WritingStyle, h_alignment=HAlignment.LEFT, v_alignment=VAlignment.TOP, save=True) -> None:
        (top_x, top_y), (width, height) = tuple_sub(result_sheet_element.start,
                                                    result_sheet.offset), result_sheet_element.size
        expression_width = DefaultPaster._get_expression_width(objects, writing_style)

        x = top_x

        if h_alignment == HAlignment.CENTER:
            x += round(((width - expression_width) / 2))
        elif h_alignment == HAlignment.RIGHT:
            x += width - expression_width
        elif h_alignment == HAlignment.RANDOM:
            x += random_int(width - expression_width)

        for _object in objects:
            if _object is None:
                x += writing_style.space_size
                continue

            y = top_y
            (character_width, character_height) = _object.size

            if v_alignment == VAlignment.CENTER:
                y += round(((height - character_height) / 2))
            elif v_alignment == VAlignment.BOTTOM:
                y += height - character_height
            elif v_alignment == VAlignment.RANDOM:
                y += random_int(height - character_height)

            if save:
                offset_x, offset_y = _object.offset
                position = (x + offset_x, y + offset_y)
                result_sheet_object = ResultSheetObject(_object.label, position, _object.size)
                result_sheet.add_object(result_sheet_element, result_sheet_object)

            result_sheet.image.paste(_object.image, (x, y), _object.mask)
            x += character_width + writing_style.distance

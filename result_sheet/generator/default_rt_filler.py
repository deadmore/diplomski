from result_sheet.config.dto import ResultSheetConfig
from result_sheet.generator.dto import HAlignment, ResultSheet, VAlignment
from result_sheet.generator.interfaces import ResultTableFiller
from result_sheet.registry.dto import ResultSheetElementType, ResultSheetImageType


class DefaultResultTableFiller(ResultTableFiller):
    __iterable_dict = {
        ResultSheetImageType.FIRST_RESULT_TABLE: lambda: range(10),
        ResultSheetImageType.SECOND_RESULT_TABLE: lambda: range(10, 20),
        ResultSheetImageType.RESULT_SHEET: lambda: range(20),
    }

    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        for index in DefaultResultTableFiller.__iterable_dict[result_sheet.type]():
            if index >= len(config.results):
                break

            rs_element = self.rs_element_registry[ResultSheetElementType.RESULT_ROW, index]
            result = config.results[index]
            paste_objects = self._get_paste_objects(result.first)

            self.paster.paste(result_sheet, rs_element, paste_objects, config.writing_style,
                              v_alignment=VAlignment.BOTTOM, h_alignment=HAlignment.RANDOM)

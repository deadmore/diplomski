from typing import List

from result_sheet.config.dto import ResultSheetConfig, WritingStyle
from result_sheet.generator.dto import HAlignment, PasteObject, ResultSheet, VAlignment
from result_sheet.registry.dto import ResultSheetElement
from result_sheet.registry.interfaces import CharacterImageRegistry, ResultSheetElementRegistry


class Paster(object):

    def paste(self, result_sheet: ResultSheet, result_sheet_element: ResultSheetElement, objects: List[PasteObject],
              writing_style: WritingStyle, h_alignment=HAlignment.LEFT, v_alignment=VAlignment.TOP, save=True):
        pass


class BaseFiller(object):

    def __init__(self, ci_registry: CharacterImageRegistry, rs_element_registry: ResultSheetElementRegistry,
                 paster: Paster):
        self.ci_registry = ci_registry
        self.rs_element_registry = rs_element_registry
        self.paster = paster

    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        pass

    def _get_paste_objects(self, name: str) -> List[PasteObject]:
        paste_objects = []

        for character in name:
            if character == " ":
                paste_objects.append(None)
                continue

            ci = self.ci_registry[character]
            paste_object = PasteObject(ci.value, ci.image, ci.mask, ci.size, ci.offset)
            paste_objects.append(paste_object)

        return paste_objects


class ResultSheetFiller(BaseFiller):
    pass


class CredentialsTableFiller(BaseFiller):
    pass


class ResultTableFiller(BaseFiller):
    pass


class FinalResultTableFiller(BaseFiller):
    pass


class InfoRowFiller(BaseFiller):
    pass

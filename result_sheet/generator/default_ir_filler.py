from enum import IntEnum

from result_sheet.config.dto import ResultSheetConfig
from result_sheet.generator.default_ct_filler import DefaultCredentialsTableFiller
from result_sheet.generator.default_frt_filler import DefaultFinalResultTableFiller
from result_sheet.generator.default_rt_filler import DefaultResultTableFiller
from result_sheet.generator.dto import ResultSheet
from result_sheet.generator.interfaces import InfoRowFiller, Paster
from result_sheet.registry.dto import ResultSheetElementType, ResultSheetImageType
from result_sheet.registry.interfaces import CharacterImageRegistry, ResultSheetElementRegistry
from util.image_util import crop
from util.tuple_util import tuple_sub


class AttributeKeys(IntEnum):
    INDEX = 1,
    OFFSET = 2,


class DefaultInfoRowFiller(InfoRowFiller):

    def __init__(self, ci_registry: CharacterImageRegistry, rs_element_registry: ResultSheetElementRegistry,
                 paster: Paster):
        super().__init__(ci_registry, rs_element_registry, paster)
        self.ct_filler = DefaultCredentialsTableFiller(ci_registry, rs_element_registry, paster)
        self.rt_filler = DefaultResultTableFiller(ci_registry, rs_element_registry, paster)
        self.frt_filler = DefaultFinalResultTableFiller(ci_registry, rs_element_registry, paster)

    # noinspection PyTypeChecker
    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        rs_image = result_sheet.rs_image
        if result_sheet.type == ResultSheetImageType.CREDENTIALS_TABLE:
            self.ct_filler.fill(result_sheet, config)
            rs_element = self.rs_element_registry[ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO, 2]
        elif result_sheet.type == ResultSheetImageType.FINAL_RESULT_TABLE:
            self.frt_filler.fill(result_sheet, config)
            rs_element = self.rs_element_registry[ResultSheetElementType.TABLE, 3]
        else:
            self.rt_filler.fill(result_sheet, config)
            index = rs_image.additional_attributes[AttributeKeys.INDEX]

            rs_element = self.rs_element_registry[ResultSheetElementType.RESULT_ROW, index]
            result_sheet.objects = {rs_element: result_sheet.objects[rs_element], }

            rs_element = self.rs_element_registry[ResultSheetElementType.FULL_RESULT_ROW, index]

        rs_image = result_sheet.rs_image
        bb_box = rs_element.bounding_box
        bb_box = tuple_sub(bb_box[0], rs_image.offset), tuple_sub(bb_box[1], rs_image.offset)
        offset = rs_image.additional_attributes[AttributeKeys.OFFSET]
        image, start = crop(result_sheet.image, bb_box, offset)

        result_sheet.rs_image.image = image

        difference = tuple_sub(bb_box[0], offset)
        for _, objects in result_sheet.objects.items():
            for _object in objects:
                _object.start = tuple_sub(_object.start, difference)

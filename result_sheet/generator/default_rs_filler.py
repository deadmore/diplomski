from result_sheet.config.dto import ResultSheetConfig
from result_sheet.generator.default_ct_filler import DefaultCredentialsTableFiller
from result_sheet.generator.default_frt_filler import DefaultFinalResultTableFiller
from result_sheet.generator.default_rt_filler import DefaultResultTableFiller
from result_sheet.generator.dto import PasteObject, ResultSheet, VAlignment
from result_sheet.generator.interfaces import Paster, ResultSheetFiller
from result_sheet.registry.dto import ResultSheetElementType
from result_sheet.registry.interfaces import CharacterImageRegistry, ResultSheetElementRegistry


class DefaultResultSheetFiller(ResultSheetFiller):

    def __init__(self, ci_registry: CharacterImageRegistry, rs_element_registry: ResultSheetElementRegistry,
                 paster: Paster):
        super().__init__(ci_registry, rs_element_registry, paster)
        self.ct_filler = DefaultCredentialsTableFiller(ci_registry, rs_element_registry, paster)
        self.rt_filler = DefaultResultTableFiller(ci_registry, rs_element_registry, paster)
        self.frt_filler = DefaultFinalResultTableFiller(ci_registry, rs_element_registry, paster)

    def _mark_checkbox(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        checkbox = self.rs_element_registry[ResultSheetElementType.CHECKBOX, config.test.test_type]
        cross = self.ci_registry["x"]

        resized = cross.image.resize(checkbox.size)
        resized_mask = cross.mask.resize(checkbox.size)

        paste_object = PasteObject("x", resized, resized_mask)

        self.paster.paste(result_sheet, checkbox, [paste_object], config.writing_style, save=False)

    def _fill_signatures(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        student_signature = self.rs_element_registry[ResultSheetElementType.SIGNATURE, 0]
        reviewer_signature = self.rs_element_registry[ResultSheetElementType.SIGNATURE, 1]

        ss_paste_objects = self._get_paste_objects(config.student.name)
        rs_paste_objects = self._get_paste_objects(config.test.reviewer_name)

        self.paster.paste(result_sheet, student_signature, ss_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, save=False)
        self.paster.paste(result_sheet, reviewer_signature, rs_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, save=False)

    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        self._mark_checkbox(result_sheet, config)
        self._fill_signatures(result_sheet, config)
        self.ct_filler.fill(result_sheet, config)
        self.rt_filler.fill(result_sheet, config)
        self.frt_filler.fill(result_sheet, config)

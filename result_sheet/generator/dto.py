from enum import IntEnum
from typing import Dict, List, Tuple

from PIL.Image import Image

from result_sheet.registry.dto import ResultSheetElement, ResultSheetImage, ResultSheetImageType
from util.tuple_util import tuple_add


class HAlignment(IntEnum):
    LEFT = 0
    CENTER = 1
    RIGHT = 2
    RANDOM = 3


class VAlignment(IntEnum):
    TOP = 0
    CENTER = 1
    BOTTOM = 2
    RANDOM = 3


class PasteObject(object):

    def __init__(self, label: str, image: Image, mask: Image, size: Tuple[int, int] = (0, 0),
                 offset: Tuple[int, int] = (0, 0)):
        self.label = label
        self.image = image
        self.mask = mask
        self.size = size
        self.offset = offset


class ResultSheetObject(object):

    def __init__(self, label: str, start: Tuple[int, int], size: Tuple[int, int]):
        self.label = label
        self.start = start
        self.size = size

    # noinspection PyTypeChecker
    @property
    def end(self) -> Tuple[int, int]:
        return tuple_add(self.start, self.size)

    @property
    def bounding_box(self) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        return self.start, self.end


class ResultSheet(object):

    def __init__(self, rs_image: ResultSheetImage, objects=None):
        if objects is None:
            objects = {}

        self.rs_image = rs_image
        self.objects: Dict[ResultSheetElement, List[ResultSheetObject]] = objects

    @property
    def image(self) -> Image:
        return self.rs_image.image

    @image.setter
    def image(self, image: Image) -> None:
        self.rs_image.image = image

    @property
    def type(self) -> ResultSheetImageType:
        return self.rs_image.type

    @property
    def index(self) -> int:
        return self.rs_image.index

    @property
    def offset(self) -> Tuple[int, int]:
        return self.rs_image.offset

    def add_object(self, result_sheet_element: ResultSheetElement, result_sheet_object: ResultSheetObject):
        if result_sheet_element not in self.objects:
            self.objects[result_sheet_element] = []

        self.objects[result_sheet_element].append(result_sheet_object)

from result_sheet.config.dto import ResultSheetConfig
from result_sheet.generator.dto import HAlignment, ResultSheet, VAlignment
from result_sheet.generator.interfaces import CredentialsTableFiller
from result_sheet.registry.dto import ResultSheetElementType


class DefaultCredentialsTableFiller(CredentialsTableFiller):

    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        subject = self.rs_element_registry[ResultSheetElementType.CREDENTIALS_TABLE_INFO, 0]
        student_name = self.rs_element_registry[ResultSheetElementType.CREDENTIALS_TABLE_INFO, 1]
        student_jmbag = self.rs_element_registry[ResultSheetElementType.CREDENTIALS_TABLE_INFO, 2]
        group = self.rs_element_registry[ResultSheetElementType.CREDENTIALS_TABLE_INFO, 3]
        date = self.rs_element_registry[ResultSheetElementType.CREDENTIALS_TABLE_INFO, 4]

        s_paste_objects = self._get_paste_objects(config.test.subject)
        sn_paste_objects = self._get_paste_objects(config.student.name)
        sj_paste_objects = self._get_paste_objects(config.student.jmbag)
        g_paste_objects = self._get_paste_objects(config.student.group)
        d_paste_objects = self._get_paste_objects(config.test.date)

        self.paster.paste(result_sheet, subject, s_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, h_alignment=HAlignment.CENTER, save=False)
        self.paster.paste(result_sheet, student_name, sn_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, save=False)
        self.paster.paste(result_sheet, student_jmbag, sj_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM)
        self.paster.paste(result_sheet, group, g_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, save=False)
        self.paster.paste(result_sheet, date, d_paste_objects, config.writing_style,
                          v_alignment=VAlignment.BOTTOM, save=False)

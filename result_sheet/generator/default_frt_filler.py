from result_sheet.config.dto import ResultSheetConfig
from result_sheet.generator.dto import ResultSheet
from result_sheet.generator.interfaces import FinalResultTableFiller
from result_sheet.registry.dto import ResultSheetElementType


class DefaultFinalResultTableFiller(FinalResultTableFiller):

    def fill(self, result_sheet: ResultSheet, config: ResultSheetConfig) -> None:
        final_result = self.rs_element_registry[ResultSheetElementType.FINAL_RESULT_ROW, 0]
        paste_objects = self._get_paste_objects(config.final_result)
        self.paster.paste(result_sheet, final_result, paste_objects, config.writing_style)

from typing import Dict, List

from result_sheet.character.dto import CharacterImage, CharacterImageType


class CharacterImageLoader(object):

    def load(self) -> Dict[CharacterImageType, List[CharacterImage]]:
        pass

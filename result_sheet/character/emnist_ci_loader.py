from typing import Callable, Dict, Iterable, List

from PIL import Image as Image_m, ImageOps
from emnist import extract_test_samples, extract_training_samples
from numpy import array as np_array, concatenate

from result_sheet.character.dto import *
from result_sheet.character.interfaces import CharacterImageLoader


class LazyCharacter(object):

    def __init__(self, character: CharacterImage, initializers: Iterable[Callable[[CharacterImage], None]]):
        self.character = character
        self.initializers = initializers
        self.initialized = False

    def __getattribute__(self, item: str):
        if item in ("initialized", "initializers", "character"):
            return object.__getattribute__(self, item)
        if not self.initialized:
            for initializer in self.initializers:
                initializer(self.character)

            self.initialized = True
            del self.initializers

        return getattr(self.character, item)


class EmnistCharacterInitializerBuilder(object):

    def build(self, array: np_array, label: str) -> Callable[[CharacterImage], None]:
        pass


class ImageAndMaskInitializerBuilder(EmnistCharacterInitializerBuilder):

    def build(self, array: np_array, label: str) -> Callable[[CharacterImage], None]:
        def initializer(character: CharacterImage):
            mask = Image_m.fromarray(array).convert('L')
            image = ImageOps.invert(mask)

            character.image = image
            character.mask = mask

        return initializer


class BoundingBoxInitializerBuilder(EmnistCharacterInitializerBuilder):

    def __init__(self, threshold: float = 0.1):
        self.threshold = threshold

    def build(self, array: np_array, label: str) -> Callable[[CharacterImage], None]:
        def initializer(character: CharacterImage):
            image_size = array.shape
            min_x, min_y = image_size[0] - 1, image_size[1] - 1
            max_x, max_y = 0, 0

            for i in range(image_size[0]):
                for j in range(image_size[1]):
                    if array[i, j] < self.threshold:
                        continue
                    else:
                        if j > max_x:
                            max_x = j
                        elif j < min_x:
                            min_x = j
                        elif i > max_y:
                            max_y = i
                        if i < min_y:
                            min_y = i

            character.offset = (min_x, min_y)
            character.size = (max_x - min_x, max_y - min_y)

        return initializer


class EmnistCharacterImageLoader(CharacterImageLoader):

    def __init__(self, builders: Iterable[EmnistCharacterInitializerBuilder] = (
            ImageAndMaskInitializerBuilder(), BoundingBoxInitializerBuilder())):
        self.builders = builders

    @staticmethod
    def get_digits() -> Tuple[Iterable[object], Iterable[object]]:
        return concatenate((extract_training_samples("digits")[0], extract_test_samples("digits")[0],
                            extract_training_samples("mnist")[0], extract_test_samples("mnist")[0])), \
               concatenate((extract_training_samples("digits")[1], extract_test_samples("digits")[1],
                            extract_training_samples("mnist")[1], extract_test_samples("mnist")[1]))

    @staticmethod
    def get_letters() -> Tuple[Iterable[object], Iterable[object]]:
        return concatenate((extract_training_samples("letters")[0], extract_test_samples("letters")[0])), \
               concatenate((extract_training_samples("letters")[1], extract_test_samples("letters")[1]))

    def load(self) -> Dict[chr, List[CharacterImage]]:
        digits, digit_labels = EmnistCharacterImageLoader.get_digits()
        letters, letter_labels = EmnistCharacterImageLoader.get_letters()

        result = {}
        characters = [(CharacterImageType.DIGIT, digits, digit_labels),
                      (CharacterImageType.LETTER, letters, letter_labels)]

        for _type, data, labels in characters:
            for image_data, label in zip(data, labels):
                if _type == CharacterImageType.LETTER:
                    label = chr(label + 96)
                else:
                    label = chr(label + 48)

                character = CharacterImage(_type, label)
                initializers = [builder.build(image_data, label) for builder in self.builders]

                lazy_character = LazyCharacter(character, initializers)

                if label not in result:
                    result[label] = [lazy_character]
                else:
                    result[label].append(lazy_character)

        return result

from enum import IntEnum
from typing import Tuple

from PIL.Image import Image


class CharacterImageType(IntEnum):
    LETTER = 0
    DIGIT = 1


class CharacterImage(object):

    def __init__(self, _type: CharacterImageType, value: chr, offset: Tuple[int, int] = (0, 0),
                 size: Tuple[int, int] = (0, 0), image: Image = None, mask: Image = None):
        self.type = _type
        self.value = value
        self.offset = offset
        self.size = size
        self.image = image
        self.mask = mask

import tkinter as tk
from tkinter.filedialog import askopenfilename

from PIL import Image
from PIL.ImageDraw import ImageDraw

from pipeline import run_eval_pipeline


class SimpleGui(object):

    def __init__(self):
        self.filename = None
        self.root = tk.Tk()
        self.root.title("Predictions")
        self.root.geometry("300x100")

        self.run_predictions_btn = tk.Button(self.root, text="Run predictions", command=self.show_predictions,
                                             state="disabled")
        self.run_predictions_btn.pack(pady=10)

        self.choose_image_btn = tk.Button(self.root, text="Choose image", command=self.choose_image)
        self.choose_image_btn.pack(pady=10)

    def show_predictions(self):
        image = Image.open(self.filename).convert('L')
        result = run_eval_pipeline(image)

        tables = result.table_predictions
        parent = tables[0].parent
        draw = ImageDraw(parent)

        for table in tables:
            points = list(map(round, table.box))
            draw.rectangle(points, outline="red", width=3)

        parent.show(title="table predictions")

        jmbag_row = result.jmbag_row_prediction
        parent = jmbag_row.parent
        draw = ImageDraw(parent)

        points = list(map(round, jmbag_row.box))
        draw.rectangle(points, outline="red", width=3)

        parent.show(title="Jmbag row prediction")

        result_rows_1 = result.result_row_predictions[:10]
        parent = result_rows_1[0].parent
        draw = ImageDraw(parent)

        for result_row in result_rows_1:
            points = list(map(round, result_row.box))
            draw.rectangle(points, outline="red", width=3)

        parent.show(title="Result row 1 predictions")

        result_rows_2 = result.result_row_predictions[10:]
        parent = result_rows_2[0].parent
        draw = ImageDraw(parent)

        for result_row in result_rows_2:
            points = list(map(round, result_row.box))
            draw.rectangle(points, outline="red", width=3)

        parent.show(title="Result row 2 predictions")

    def choose_image(self):
        self.filename = askopenfilename()
        if self.filename:
            self.run_predictions_btn["state"] = "normal"

    def run(self):
        tk.mainloop()


if __name__ == '__main__':
    SimpleGui().run()

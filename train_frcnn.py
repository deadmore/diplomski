import torch.utils.data

from detection.frcnn.engine import train_one_epoch
from detection.frcnn.frcnn_model import faster_rcnn, save_model
from detection.frcnn.util import collate_fn
from util.dataset_util import get_dd_dataset

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
class_count = 1

if __name__ == '__main__':
    torch.cuda.empty_cache()
    dataset = get_dd_dataset(epoch_size=5000)

    data_loader = torch.utils.data.DataLoader(dataset, batch_size=8, shuffle=False, collate_fn=collate_fn)

    model = faster_rcnn(class_count)
    model.to(device)

    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=0.01, momentum=0.9, weight_decay=0.0005)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.5)

    for epoch in range(101):
        train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq=10)
        lr_scheduler.step()
        if epoch > 0 and (epoch + 1) % 10 == 0:
            save_model(model, f'weights/dd/greyscale_dd_2_{epoch + 1}.weights')

    save_model(model, 'weights/dd/greyscale_dd_2_final.weights')

import numpy as np
import torch.nn.functional as F
import torch.utils.data
from matplotlib import pyplot as plt
from torchvision import datasets
from torchvision.transforms import ToTensor

from detection.frcnn.frcnn_model import load_model
from util.dataset_util import get_mnist_test_dataset


def test():
    device = ("cuda" if torch.cuda.is_available() else "cpu")
    model = load_model('weights/mnist/greyscale_final.weights')
    test_set = datasets.MNIST('data/', download=True, train=False, transform=ToTensor())
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=64, shuffle=True)

    # batch_idx, (images, labels) = next(enumerate(test_loader))
    # img = images[0]
    img, label = get_mnist_test_dataset()[0]
    img = img.to(device)
    img = img.view(-1, 1, 28, 28)

    # Since we want to use the already pretrained weights to make some prediction
    # we are turning off the gradients
    with torch.no_grad():
        logits = model.forward(img)

    probabilities = F.softmax(logits, dim=1).detach().cpu().numpy().squeeze()

    fig, (ax1, ax2) = plt.subplots(figsize=(6, 8), ncols=2)
    ax1.imshow(img.view(1, 28, 28).detach().cpu().numpy().squeeze(), cmap='inferno')
    ax1.axis('off')
    ax2.barh(np.arange(10), probabilities, color='r')
    ax2.set_aspect(0.1)
    ax2.set_yticks(np.arange(10))
    ax2.set_yticklabels(np.arange(10))
    ax2.set_title('Class Probability')
    ax2.set_xlim(0, 1.1)

    plt.tight_layout()
    fig.show()


if __name__ == '__main__':
    test()

import torch

from util.dataset_util import get_test_dc_dataset
from util.extraction_util import detect_digits
from util.metrics_util import MetricType, get_confusion_matrix2
from util.model_registry import dc_model

if __name__ == '__main__':
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = dc_model()
    dataset = get_test_dc_dataset()
    hits = 0
    confusion_matrix = None

    for i in range(1000):
        print(i)
        value = dataset[0]
        images = list(map(lambda x: x[0], value))
        labels = list(map(lambda x: x[1], value))

        jmbag = detect_digits(images, model, device, invert=False)
        other = get_confusion_matrix2(labels, jmbag)

        if confusion_matrix is None:
            confusion_matrix = other
        else:
            confusion_matrix += other

    for i in range(10):
        print("{}        & {:.6f}        & {:.6f}   & {:.6f}       \\ \hline".format(i,
                                                                                     confusion_matrix.precision(i),
                                                                                     confusion_matrix.recall(i),
                                                                                     confusion_matrix.f1_score(i)))
    print("macro       & {:.3f}        & {:.3f}   & {:.3f}       \\ \hline".format(i,
                                                                                   confusion_matrix.macro(
                                                                                       MetricType.PRECISION),
                                                                                   confusion_matrix.macro(
                                                                                       MetricType.RECALL),
                                                                                   confusion_matrix.macro(
                                                                                       MetricType.F1)))
    print("macro       & {:.3f}        & {:.3f}   & {:.3f}       \\ \hline".format(i,
                                                                                   confusion_matrix.micro(
                                                                                       MetricType.PRECISION),
                                                                                   confusion_matrix.micro(
                                                                                       MetricType.RECALL),
                                                                                   confusion_matrix.micro(
                                                                                       MetricType.F1)))
    print("hej")

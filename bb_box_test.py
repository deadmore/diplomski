from util.dataset_util import get_test_ir_dataset

if __name__ == "__main__":
    dataset = get_test_ir_dataset(draw_bb=True)
    image = dataset[0]
    image.show()

from typing import List

import torch
from PIL.Image import Image as PImage

from util.extraction_util import Prediction, detect_digits, extract_jmbag, extract_jmbag_row, extract_result, \
    extract_result_rows, \
    extract_tables
from util.model_registry import ct_model, dc_model, dd_model, rs_model, rt_model
from util.time_util import timed


class PredictionResult(object):

    def __init__(self, table_predictions: List[Prediction], jmbag_row_prediction: Prediction,
                 result_row_predictions: List[Prediction], jmbag: List[int], results: List[List[int]]):
        self.table_predictions = table_predictions
        self.jmbag_row_prediction = jmbag_row_prediction
        self.result_row_predictions = result_row_predictions
        self.jmbag = jmbag
        self.results = results


def __to_images(predictions: List[Prediction]) -> List[PImage]:
    return list(map(lambda x: x.image, predictions))


def run_eval_pipeline(image: PImage) -> PredictionResult:
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    model = rs_model()
    model.to(device)

    tables = extract_tables(image, model, device)
    del model

    model = ct_model()
    model.to(device)

    jmbag_row = extract_jmbag_row(tables[0].image, model, device)
    del model

    model = rt_model()
    model.to(device)

    result_rows = extract_result_rows(tables[1].image, model, device) + \
                  extract_result_rows(tables[2].image, model, device)
    del model

    model = dd_model()
    model.to(device)

    detected_jmbag = extract_jmbag(jmbag_row.image, model, device)
    detected_results = []

    for result_row in result_rows:
        detected_result = extract_result(result_row.image, model, device)
        detected_results.append(detected_result)

    del model

    model = dc_model()
    model.to(device)

    detected_jmbag = __to_images(detected_jmbag)
    jmbag = detect_digits(detected_jmbag, model, device)

    results = []
    for detected_result in detected_results:
        detected_result = __to_images(detected_result)
        result = detect_digits(detected_result, model, device)
        results.append(result)

    return PredictionResult(tables, jmbag_row, result_rows, jmbag, results)


@timed
def timed_run_eval_pipeline(image: PImage):
    return run_eval_pipeline(image)

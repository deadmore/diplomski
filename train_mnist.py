import torch.utils.data
from torch import nn
from torchvision import datasets
from torchvision.transforms import ToTensor

from detection.frcnn.frcnn_model import save_model
from detection.mnist.CNN import CNN


def train():
    device = ("cuda" if torch.cuda.is_available() else "cpu")

    train_set = datasets.MNIST(
        root='data',
        train=True,
        transform=ToTensor(),
        download=True,
    )
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=128, shuffle=True)

    test_set = datasets.MNIST('data/', download=True, train=False, transform=ToTensor())
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=64, shuffle=True)

    epochs = 20
    model = CNN()
    model.to(device)

    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=0.01, momentum=0.9, weight_decay=0.0005)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.5)
    criterion = nn.CrossEntropyLoss()

    train_losses = []
    test_losses = []

    for epoch in range(epochs):
        # prep model for training
        model.train()
        train_loss = 0

        for idx, (images, labels) in enumerate(train_loader):

            # Send these >>> To GPU
            images = images.to(device)
            labels = labels.to(device)

            # Training pass
            optimizer.zero_grad()

            # Forward pass
            output = model(images)
            loss = criterion(output, labels)

            # Backward pass
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        else:
            # prep model for evaluation
            model.eval()
            test_loss = 0
            accuracy = 0

            # Turn off the gradients when performing validation.
            # If we don't turn it off, we will comprise our networks weight entirely
            with torch.no_grad():
                for images, labels in test_loader:
                    images = images.to(device)
                    labels = labels.to(device)

                    log_probabilities = model(images)
                    test_loss += criterion(log_probabilities, labels)

                    probabilities = torch.exp(log_probabilities)
                    top_prob, top_class = probabilities.topk(1, dim=1)
                    predictions = top_class == labels.view(*top_class.shape)
                    accuracy += torch.mean(predictions.type(torch.FloatTensor))

            train_losses.append(train_loss / len(train_loader))
            test_losses.append(test_loss / len(test_loader))

            print("Epoch: {}/{}  ".format(epoch + 1, epochs),
                  "Training loss: {:.4f}  ".format(train_loss / len(train_loader)),
                  "Testing loss: {:.4f}  ".format(test_loss / len(test_loader)),
                  "Test accuracy: {:.4f}  ".format(accuracy / len(test_loader)))
        if (epoch + 1) % 5 == 0:
            save_model(model, 'weights/mnist/greyscale_mnist_{}.weights'.format(epoch))

        lr_scheduler.step()

    save_model(model, 'weights/mnist/greyscale_mnist_final.weights')


if __name__ == '__main__':
    train()

import operator
from typing import Callable, Tuple, TypeVar

T = TypeVar('T')


def tuple_add(tuple1: Tuple[T, ...], tuple2: Tuple[T, ...]) -> Tuple[T, ...]:
    return _do_tuple_operation(tuple1, tuple2, operation=operator.add)


def tuple_sub(tuple1: Tuple[T, ...], tuple2: Tuple[T, ...]) -> Tuple[T, ...]:
    return _do_tuple_operation(tuple1, tuple2, operation=operator.sub)


def _do_tuple_operation(tuple1: Tuple[T, ...], tuple2: Tuple[T, ...],
                        operation: Callable[[T, T], T]) -> Tuple[T, ...]:
    return tuple(map(operation, tuple1, tuple2))

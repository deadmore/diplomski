from detection.dataset.aspect_transform import AspectTransform
from detection.dataset.base_dataset import BaseDataset
from detection.dataset.ct_dataset import CredentialsTableDataset
from detection.dataset.dc_dataset import DigitClassificationDataset
from detection.dataset.dc_transform import DigitDetectionTransform
from detection.dataset.dd_dataset import DigitDetectionDataset
from detection.dataset.draw_bb_transform import DrawBoundingBoxTransform
from detection.dataset.frcnn_transform import FasterRCNNTransform
from detection.dataset.ir_dataset import InfoRowDataset
from detection.dataset.mutate_image_transform import MutateImageTransform
from detection.dataset.random_rotate_transform import RandomRotateTransform
from detection.dataset.row_transform import JmbagRowTransform, ResultRowTransform
from detection.dataset.rs_dataset import ResultSheetDataset
from detection.dataset.rs_table_transform import ResultSheetTableTransform
from detection.dataset.rt_dataset import ResultTableDataset
from result_sheet.character.emnist_ci_loader import EmnistCharacterImageLoader, Tuple
from result_sheet.config.interfaces import ResultSheetConfigGenerator
from result_sheet.config.random_rs_config_generator import RandomResultSheetConfigGenerator
from result_sheet.config.real_rs_config_generator import RealResultSheetConfigGenerator
from result_sheet.generator.default_ct_filler import DefaultCredentialsTableFiller
from result_sheet.generator.default_ir_filler import DefaultInfoRowFiller
from result_sheet.generator.default_paster import DefaultPaster
from result_sheet.generator.default_rs_filler import DefaultResultSheetFiller
from result_sheet.generator.default_rt_filler import DefaultResultTableFiller
from result_sheet.generator.interfaces import CredentialsTableFiller, InfoRowFiller, ResultSheetFiller, \
    ResultTableFiller
from result_sheet.registry.default_rs_element_registry import DefaultResultSheetElementRegistry
from result_sheet.registry.default_rs_image_registry import DefaultResultSheetImageRegistry
from result_sheet.registry.interfaces import CharacterImageRegistry, ResultSheetImageRegistry
from result_sheet.registry.random_ci_registry import RandomCharacterImageRegistry
from util.time_util import timed

rs_label_dict = {
    "credentials_table": 0,
    "result_table": 1,
    "final_result_table": 2,
}

rt_label_dict = {
    "result_row": 0,
}

ct_label_dict = {
    "jmbag_row": 0,
}

ir_label_dict = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
}

dd_label_dict = {
    "0": 0,
    "1": 0,
    "2": 0,
    "3": 0,
    "4": 0,
    "5": 0,
    "6": 0,
    "7": 0,
    "8": 0,
    "9": 0,
}

merged_label_dict = {
    **rs_label_dict, **rt_label_dict, **ct_label_dict, **dd_label_dict
}


def get_rs_dataset_config() -> Tuple[ResultSheetImageRegistry, ResultSheetConfigGenerator, ResultSheetFiller]:
    result_sheet_path = "./resources/kosuljice-slike/kosuljica-1-0.jpg"

    config_generator = RandomResultSheetConfigGenerator()
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)
    rs_element_registry = DefaultResultSheetElementRegistry()
    rs_image_registry = DefaultResultSheetImageRegistry(result_sheet_path, rs_element_registry)
    paster = DefaultPaster()
    rs_filler = DefaultResultSheetFiller(ci_registry, rs_element_registry, paster)

    return rs_image_registry, config_generator, rs_filler


def get_rt_dataset_config() -> Tuple[ResultSheetImageRegistry, ResultSheetConfigGenerator, ResultTableFiller]:
    result_sheet_path = "./resources/kosuljice-slike/kosuljica-1-0.jpg"

    config_generator = RandomResultSheetConfigGenerator()
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)
    rs_element_registry = DefaultResultSheetElementRegistry()
    rs_image_registry = DefaultResultSheetImageRegistry(result_sheet_path, rs_element_registry)
    paster = DefaultPaster()
    rt_filler = DefaultResultTableFiller(ci_registry, rs_element_registry, paster)

    return rs_image_registry, config_generator, rt_filler


def get_ct_dataset_config() -> Tuple[ResultSheetImageRegistry, ResultSheetConfigGenerator, CredentialsTableFiller]:
    result_sheet_path = "./resources/kosuljice-slike/kosuljica-1-0.jpg"

    config_generator = RandomResultSheetConfigGenerator()
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)
    rs_element_registry = DefaultResultSheetElementRegistry()
    rs_image_registry = DefaultResultSheetImageRegistry(result_sheet_path, rs_element_registry)
    paster = DefaultPaster()
    ct_filler = DefaultCredentialsTableFiller(ci_registry, rs_element_registry, paster)

    return rs_image_registry, config_generator, ct_filler


def get_ir_dataset_config() -> Tuple[ResultSheetImageRegistry, ResultSheetConfigGenerator, InfoRowFiller]:
    result_sheet_path = "./resources/kosuljice-slike/kosuljica-1-0.jpg"

    config_generator = RandomResultSheetConfigGenerator()
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)
    rs_element_registry = DefaultResultSheetElementRegistry()
    rs_image_registry = DefaultResultSheetImageRegistry(result_sheet_path, rs_element_registry)
    paster = DefaultPaster()
    ir_filler = DefaultInfoRowFiller(ci_registry, rs_element_registry, paster)

    return rs_image_registry, config_generator, ir_filler


def get_digit_dataset_config() -> CharacterImageRegistry:
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)

    return ci_registry


def get_pipeline_config() -> Tuple[ResultSheetImageRegistry, ResultSheetConfigGenerator, ResultSheetFiller]:
    result_sheet_path = "./resources/kosuljice-slike/kosuljica-1-0.jpg"

    config_generator = RealResultSheetConfigGenerator()
    ci_loader = EmnistCharacterImageLoader()
    ci_registry = RandomCharacterImageRegistry(ci_loader)
    rs_element_registry = DefaultResultSheetElementRegistry()
    rs_image_registry = DefaultResultSheetImageRegistry(result_sheet_path, rs_element_registry)
    paster = DefaultPaster()
    rs_filler = DefaultResultSheetFiller(ci_registry, rs_element_registry, paster)

    return rs_image_registry, config_generator, rs_filler


@timed
def get_rs_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, rs_filler = get_rs_dataset_config()
    rs_element_registry = rs_filler.rs_element_registry
    transforms = [ResultSheetTableTransform(rs_element_registry), RandomRotateTransform(max_angle=1),
                  MutateImageTransform(), FasterRCNNTransform(rs_label_dict)]

    dataset = ResultSheetDataset(rs_image_registry, config_generator, rs_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_rt_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, rt_filler = get_rt_dataset_config()
    rs_element_registry = rt_filler.rs_element_registry
    transforms = [ResultRowTransform(rs_element_registry), RandomRotateTransform(max_angle=1),
                  MutateImageTransform(), FasterRCNNTransform(rt_label_dict)]

    dataset = ResultTableDataset(rs_image_registry, config_generator, rt_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_ct_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, ct_filler = get_ct_dataset_config()
    rs_element_registry = ct_filler.rs_element_registry
    transforms = [JmbagRowTransform(rs_element_registry), RandomRotateTransform(max_angle=1),
                  MutateImageTransform(), FasterRCNNTransform(ct_label_dict)]

    dataset = CredentialsTableDataset(rs_image_registry, config_generator, ct_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_ir_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, ir_filler = get_ir_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), FasterRCNNTransform(ir_label_dict)]

    dataset = InfoRowDataset(rs_image_registry, config_generator, ir_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_dd_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, ir_filler = get_ir_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), AspectTransform(),
                  FasterRCNNTransform(dd_label_dict)]

    dataset = InfoRowDataset(rs_image_registry, config_generator, ir_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_dd_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, ir_filler = get_ir_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), AspectTransform(),
                  FasterRCNNTransform(dd_label_dict)]

    dataset = InfoRowDataset(rs_image_registry, config_generator, ir_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_dc_dataset(epoch_size=1000) -> BaseDataset:
    rs_image_registry, config_generator, ct_filler = get_ct_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), DigitDetectionTransform()]

    dataset = DigitClassificationDataset(rs_image_registry, config_generator, ct_filler, transforms=transforms)
    dataset.epoch_size = epoch_size

    return dataset


@timed
def get_mnist_test_dataset() -> DigitDetectionDataset:
    ci_registry = get_digit_dataset_config()
    dataset = DigitDetectionDataset(ci_registry)

    return dataset


@timed
def get_test_rs_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, rs_filler = get_rs_dataset_config()
    rs_element_registry = rs_filler.rs_element_registry
    transforms = [ResultSheetTableTransform(rs_element_registry), RandomRotateTransform(max_angle=1)]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return ResultSheetDataset(rs_image_registry, config_generator, rs_filler, transforms=transforms)


@timed
def get_test_rt_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, rt_filler = get_rt_dataset_config()
    rs_element_registry = rt_filler.rs_element_registry
    transforms = [ResultRowTransform(rs_element_registry), RandomRotateTransform(max_angle=1), MutateImageTransform()]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return ResultTableDataset(rs_image_registry, config_generator, rt_filler, transforms=transforms)


@timed
def get_test_ct_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, ct_filler = get_ct_dataset_config()
    rs_element_registry = ct_filler.rs_element_registry
    transforms = [JmbagRowTransform(rs_element_registry), RandomRotateTransform(max_angle=1), MutateImageTransform()]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return CredentialsTableDataset(rs_image_registry, config_generator, ct_filler, transforms=transforms)


@timed
def get_test_ir_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, ir_filler = get_ir_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform()]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return InfoRowDataset(rs_image_registry, config_generator, ir_filler, transforms=transforms)


@timed
def get_test_dd_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, ir_filler = get_ir_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), AspectTransform()]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return InfoRowDataset(rs_image_registry, config_generator, ir_filler, transforms=transforms)


@timed
def get_test_dc_dataset(draw_bb=False) -> BaseDataset:
    rs_image_registry, config_generator, ct_filler = get_ct_dataset_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform(), DigitDetectionTransform(to_tensor=False)]

    if draw_bb:
        transforms.insert(len(transforms) - 2, DrawBoundingBoxTransform())

    return DigitClassificationDataset(rs_image_registry, config_generator, ct_filler, transforms=transforms)


@timed
def get_pipeline_dataset() -> BaseDataset:
    rs_image_registry, config_generator, rs_filler = get_pipeline_config()
    transforms = [RandomRotateTransform(max_angle=1), MutateImageTransform()]

    return ResultSheetDataset(rs_image_registry, config_generator, rs_filler, transforms=transforms)

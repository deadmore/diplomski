from enum import Enum
from functools import lru_cache
from typing import List

from util.label_util import Label
from util.types import Box


class MetricType(Enum):
    PRECISION = 'precision',
    ACCURACY = 'accuracy',
    F1 = 'f1_score',
    RECALL = 'recall',


class ConfusionMatrix(object):

    def __init__(self, data: List[List[int]], N: int, false_positives: List[int], false_negatives: List[int]):
        self.data = data
        self.N = N
        self.false_negatives = false_negatives
        self.false_positives = false_positives

    @lru_cache(maxsize=None)
    def tp(self, clazz: int) -> int:
        return self.data[clazz][clazz]

    @lru_cache(maxsize=None)
    def fp(self, clazz: int) -> int:
        result = self.false_positives[clazz]
        row = self.data[clazz]

        for i in range(len(self.data)):
            if i == clazz:
                break

            result += row[i]

        return result

    @lru_cache(maxsize=None)
    def fn(self, clazz: int) -> int:
        result = self.false_negatives[clazz]
        for i in range(len(self.data)):
            if i == clazz:
                break

            result += self.data[i][clazz]

        return result

    @lru_cache(maxsize=None)
    def tn(self, clazz: int) -> int:
        return self.N - self.tp(clazz) - self.fp(clazz) - self.fn(clazz) + self.false_negatives[clazz] + \
               self.false_positives[clazz]

    def class_count(self):
        return len(self.data)

    @lru_cache(maxsize=None)
    def precision(self, clazz: int) -> float:
        tp = self.tp(clazz)
        fp = self.fp(clazz)

        return precision(tp, fp)

    @lru_cache(maxsize=None)
    def recall(self, clazz: int) -> float:
        tp = self.tp(clazz)
        fn = self.fn(clazz)

        return recall(tp, fn)

    @lru_cache(maxsize=None)
    def f1_score(self, clazz: int) -> float:
        tp = self.tp(clazz)
        fp = self.fp(clazz)
        fn = self.fn(clazz)

        return f1_score(tp, fp, fn)

    @lru_cache(maxsize=None)
    def accuracy(self, clazz: int) -> float:
        tp = self.tp(clazz)
        fp = self.fp(clazz)
        tn = self.tn(clazz)
        fn = self.fn(clazz)

        return accuracy(tp, fp, tn, fn)

    @lru_cache(maxsize=None)
    def macro(self, metric: MetricType) -> float:
        result = 0
        class_count = self.class_count()
        method = getattr(self, metric.value[0])

        for i in range(class_count):
            result += method(i)

        return result / class_count

    @lru_cache(maxsize=None)
    def micro(self, metric: MetricType) -> float:
        tp = 0
        fp = 0
        tn = 0
        fn = 0

        for i in range(self.class_count()):
            tp += self.tp(i)
            fp += self.fp(i)
            tn += self.tn(i)
            fn += self.fn(i)

        if metric == MetricType.PRECISION:
            return precision(tp, fp)
        elif metric == MetricType.RECALL:
            return recall(tp, fn)
        elif metric == MetricType.F1:
            return f1_score(tp, fp, fn)

        return accuracy(tp, fp, tn, fn)

    def __add__(self, other):
        data = []
        false_positives = []
        false_negatives = []
        N = self.N + other.N

        for i in range(self.class_count()):
            row = []
            data.append(row)

            false_positives.append(self.false_positives[i] + other.false_positives[i])
            false_negatives.append(self.false_negatives[i] + other.false_negatives[i])

            for j in range(self.class_count()):
                row.append(self.data[i][j] + other.data[i][j])

        return ConfusionMatrix(data, N, false_positives, false_negatives)


def get_confusion_matrix(class_count: int, labels: List[Label], predictions: List[object], threshold=0.8) \
        -> ConfusionMatrix:
    labels = labels.copy()
    false_positives = [0 for x in range(class_count)]
    false_negatives = [0 for x in range(class_count)]
    confusion_matrix = [[0 for x in range(class_count)] for x in range(class_count)]

    for prediction in predictions:
        for label in labels:
            if iou(label.box, prediction.box) > threshold:
                labels.remove(label)
                confusion_matrix[label.clazz][prediction.clazz] += 1
                break
        else:
            false_positives[prediction.clazz] += 1

    for label in labels:
        false_negatives[label.clazz] += 1

    return ConfusionMatrix(confusion_matrix, len(predictions), false_positives, false_negatives)


def get_confusion_matrix2(labels: List[int], predictions: List[int]) -> ConfusionMatrix:
    class_count = 10
    false_positives = [0 for x in range(class_count)]
    false_negatives = [0 for x in range(class_count)]
    confusion_matrix = [[0 for x in range(class_count)] for x in range(class_count)]

    for label, prediction in zip(labels, predictions):
        confusion_matrix[label][prediction] += 1

    return ConfusionMatrix(confusion_matrix, len(predictions), false_positives, false_negatives)


def iou(box1: Box, box2: Box) -> float:
    x_a = max(box1[0], box2[0])
    y_a = max(box1[1], box2[1])
    x_b = min(box1[2], box2[2])
    y_b = min(box1[3], box2[3])

    intersection_area = max(0., x_b - x_a + 1.) * max(0., y_b - y_a + 1.)
    box_a_area = (box1[2] - box1[0] + 1) * (box1[3] - box1[1] + 1)
    box_b_area = (box2[2] - box2[0] + 1) * (box2[3] - box2[1] + 1)

    return intersection_area / float(box_a_area + box_b_area - intersection_area)


def precision(tp: int, fp: int) -> float:
    r = tp + fp

    if r == 0:
        return 1

    return tp / r


def recall(tp, fn) -> float:
    r = tp + fn

    if r == 0:
        return 1

    return tp / r


def f1_score(tp: int, fp: int, fn: int) -> float:
    p = precision(tp, fp)
    r = recall(tp, fn)

    if p == 0 or r == 0:
        return 0

    return 2 * p * r / (p + r)


def accuracy(tp: int, fp: int, tn: int, fn: int) -> float:
    p = tp + tn
    r = p + fp + fn

    if r == 0:
        return 0

    return p / r

from typing import Tuple

from PIL import Image, ImageOps
from PIL.Image import Image as PImage

from util.tuple_util import tuple_add, tuple_sub


def crop(image: PImage, box: Tuple[Tuple[int, int], Tuple[int, int]], offset: Tuple[int, int]) \
        -> Tuple[PImage, Tuple[int, int]]:
    start, end = tuple_sub(box[0], offset), tuple_add(box[1], offset)
    # noinspection PyTypeChecker
    return image.crop((*start, *end)), start


def resize_and_invert_digit(image: PImage) -> PImage:
    width, height = image.size
    new_width, new_height = 28, 28

    offset_x = (new_width - width) // 2
    offset_y = (new_height - height) // 2

    result = Image.new(image.mode, (new_width, new_height), color='white')
    result.paste(image, (offset_x, offset_y))
    result = ImageOps.invert(result)

    return result


def resize_width(image: PImage, new_width=665, color='white') -> Tuple[PImage, int]:
    width, height = image.size
    new_height = height
    result = Image.new(image.mode, (new_width, new_height), color)
    offset = (new_width - width) // 2
    result.paste(image, (offset, 0))

    return result, offset

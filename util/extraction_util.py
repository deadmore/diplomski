from enum import IntEnum
from itertools import islice
from typing import Callable, Iterator, List

import torch
from PIL.Image import Image as PImage
from torch.utils.data.dataloader import default_collate
from torchvision.transforms.functional import to_tensor

from util.image_util import resize_and_invert_digit, resize_width
from util.metrics_util import iou
from util.types import Box, RawPrediction

DEFAULT_MIN_SCORE = 0.8
DEFAULT_NON_MAX_SUPPRESSION_THRESHOLD = 0.5
DEFAULT_PROCESS_CROP_BOX = lambda box: box


class Prediction(object):

    def __init__(self, parent: PImage, image: PImage, box: Box, clazz: int, score: float):
        self.parent = parent
        self.image = image
        self.box = box
        self.clazz = clazz
        self.score = score
        self.parent = parent


class PredictionErrorType(IntEnum):
    TOO_FEW_TABLES_FOUND = 0
    TOO_MANY_TABLES_FOUND = 1

    NO_JMBAG_ROW_FOUND = 2

    TOO_FEW_JMBAG_DIGITS_FOUND = 3

    TOO_FEW_RESULT_ROWS_FOUND = 4


class PredictionError(Exception):

    def __init__(self, _type: PredictionErrorType, message: str):
        self.type = _type
        self.message = message


def __non_max_suppression(predictions: List[Prediction], threshold=0.5) -> List[Prediction]:
    if len(predictions) == 0:
        return []

    sorted_predictions = list(sorted(predictions, key=__score_key, reverse=True))
    remove = set()

    for i in range(len(sorted_predictions)):
        prediction = sorted_predictions[i]

        if prediction in remove:
            continue

        for j in range(i + 1, len(sorted_predictions)):
            other_prediction = sorted_predictions[j]

            if iou(prediction.box, other_prediction.box) >= threshold:
                remove.add(other_prediction)

    def filter_function(value: Prediction):
        return value not in remove

    return list(filter(filter_function, sorted_predictions))


def __raw_predictions(value) -> Iterator[RawPrediction]:
    return zip(value["boxes"].tolist(), value["labels"].tolist(), value["scores"].tolist())


def __x_axis_key(value: Prediction):
    return value.box[0]


def __y_axis_key(value: Prediction):
    return value.box[1]


def __score_key(value: Prediction):
    return value.score


def __extract_targets(image, model, device, min_score=0., key_func=None, reverse=False,
                      nms_threshold=DEFAULT_NON_MAX_SUPPRESSION_THRESHOLD, process_crop_box=DEFAULT_PROCESS_CROP_BOX) \
        -> List[Prediction]:
    model.eval()
    with torch.no_grad():
        def to_prediction(value: RawPrediction):
            box, label, score = value
            box = process_crop_box(box)

            int_box = list(map(round, box))
            cropped_image = image.crop(int_box)

            return Prediction(image, cropped_image, box, int(label), score)

        def score_filter(raw_prediction):
            return raw_prediction[2] >= min_score

    results = list(map(to_prediction, filter(score_filter, __raw_predictions(model([to_tensor(image).to(device)])[0]))))
    results = __non_max_suppression(results, nms_threshold)

    if key_func is not None:
        results = sorted(results, key=key_func, reverse=reverse)

    return list(results)


def trim_recovery(expected: int) -> Callable[[List[Prediction]], List[Prediction]]:
    def recovery(predictions):
        return list(islice(sorted(predictions, key=__score_key, reverse=True), expected))

    return recovery


def average_result_row_recovery(predictions: List[Prediction]) -> List[Prediction]:
    sorted_predictions = list(sorted(predictions, key=__y_axis_key))
    length = len(predictions)
    average_height = 0.

    def calculate_height(value: Prediction) -> float:
        return value.box[3] - value.box[1]

    for i in range(length):
        prediction = sorted_predictions[i]
        average_height += calculate_height(prediction)

    average_height /= length

    results = []
    height = average_height
    starting_index = 0

    while len(results) != 10:
        min_distance = 1000000000

        for index in range(starting_index, length):
            prediction = sorted_predictions[index]
            distance = abs(height - prediction.box[1])

            if distance > min_distance:
                break

            min_distance = distance
            starting_index = index + 1

        results.append(sorted_predictions[starting_index - 1])

    return results


def process_crop_digits(box: Box) -> Box:
    min_x, min_y, max_x, max_y = box
    width, height = max_x - min_x, max_y - min_y

    if width < 26:
        min_x -= 1
        max_x += 1

    if height < 26:
        min_y -= 1
        max_y += 1

    return min_x, min_y, max_x, max_y


def extract_tables(image, model, device, min_score=DEFAULT_MIN_SCORE, recovery=trim_recovery(4)) -> List[Prediction]:
    results = __extract_targets(image, model, device, min_score, key_func=__y_axis_key)
    if len(results) < 4:
        raise PredictionError(PredictionErrorType.TOO_FEW_TABLES_FOUND, 'Too few tables were found')
    elif len(results) > 4:
        results = recovery(results)

    if results[1].box[0] > results[2].box[0]:
        results[1], results[2] = results[2], results[1]

    return results


def extract_jmbag_row(image, model, device, min_score=DEFAULT_MIN_SCORE, recovery=trim_recovery(1)) -> Prediction:
    results = __extract_targets(image, model, device, min_score, key_func=__y_axis_key)
    if len(results) < 1:
        raise PredictionError(PredictionErrorType.NO_JMBAG_ROW_FOUND, 'No JMBAG row was found')
    elif len(results) > 1:
        results = recovery(results)

    return results[0]


def extract_result_rows(image, model, device, min_score=DEFAULT_MIN_SCORE, recovery=average_result_row_recovery) -> \
        List[Prediction]:
    results = __extract_targets(image, model, device, min_score)
    if len(results) < 10:
        raise PredictionError(PredictionErrorType.TOO_FEW_RESULT_ROWS_FOUND, 'Too few result rows were found')
    elif len(results) > 10:
        results = recovery(results)

    return list(sorted(results, key=__y_axis_key))


def extract_result(image, model, device, min_score=DEFAULT_MIN_SCORE) -> List[Prediction]:
    result, _ = resize_width(image)
    return __extract_targets(result, model, device, min_score, key_func=__x_axis_key,
                             process_crop_box=process_crop_digits)


def extract_jmbag(image, model, device, min_score=DEFAULT_MIN_SCORE, recovery=trim_recovery(10)) -> List[Prediction]:
    results = __extract_targets(image, model, device, min_score, process_crop_box=process_crop_digits)
    if len(results) < 10:
        raise PredictionError(PredictionErrorType.TOO_FEW_JMBAG_DIGITS_FOUND, 'Too few JMBAG digits were found')
    elif len(results) > 10:
        results = recovery(results)

    return list(sorted(results, key=__x_axis_key))


def detect_digits(images, model, device, invert=True) -> List[int]:
    if len(images) == 0:
        return []

    inverted_images = []

    for image in images:
        if invert:
            inverted = resize_and_invert_digit(image)
        else:
            inverted = image

        tensor = to_tensor(inverted)
        tensor = tensor.to(device)
        inverted_images.append(tensor)

    with torch.no_grad():
        inverted_images_tensor = default_collate(inverted_images)
        log_probabilities = model(inverted_images_tensor)
        probabilities = torch.exp(log_probabilities)
        _, top_classes = probabilities.topk(1, dim=1)

    results = []
    for top_class in top_classes:
        results.append(top_class.item())

    return results

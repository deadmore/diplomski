from detection.frcnn.frcnn_model import load_model


def rs_model():
    return load_model("weights/table/greyscale_rs_40.weights")


def ct_model():
    return load_model("weights/jmbag_row/greyscale_rt_30.weights")


def rt_model():
    return load_model("weights/result_row/greyscale_rt_30.weights")


def dd_model():
    return load_model("weights/dd/greyscale_dd_2_60.weights")


def dc_model():
    return load_model('weights/dc/greyscale_dc_2_49.weights')

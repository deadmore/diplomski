from os import path

from pdf2image import convert_from_path


def convert_to_image(source: str, destination_dir: str, base_name: str = "kosuljica") -> None:
    pages = convert_from_path(source, 100)

    for index, page in enumerate(pages):
        destination = path.join(destination_dir, base_name + "-{}.jpg".format(index))
        page.save(destination)


if __name__ == "__main__":
    result = path.dirname(".")
    convert_to_image("resources/kosuljice-pdf/18101911_56_00.pdf", "kosuljice-slike", "kosuljica-1")
    convert_to_image("resources/kosuljice-pdf/18101911_58_14.pdf", "kosuljice-slike", "kosuljica-2")

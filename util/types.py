from typing import Tuple

Box = Tuple[float, float, float, float]
RawPrediction = Tuple[Box, str, float]

import time
from typing import Callable


def __local_to_utc(seconds):
    return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(seconds))


def timed(fn: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        start = time.time()
        print(f'{fn.__name__} start: {__local_to_utc(start)}')
        result = fn(*args, **kwargs)
        end = time.time()
        print(f'{fn.__name__} end: {__local_to_utc(end)} duration: {end - start}')

        return result

    return wrapper

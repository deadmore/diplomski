from typing import Dict, List, Tuple

from result_sheet.generator.dto import ResultSheet, ResultSheetObject
from result_sheet.registry.dto import ResultSheetElementType
from util.types import Box


class Label(object):

    def __init__(self, clazz: int, box: Box):
        self.clazz = clazz
        self.box = box


class InfoLabels(object):

    def __init__(self, jmbag_labels: List[Label], result_labels: List[List[Label]], final_result_labels: List[Label]):
        self.jmbag_labels = jmbag_labels
        self.result_labels = result_labels
        self.final_result_labels = final_result_labels


def labels_to_classes(labels: List[Label]) -> List[int]:
    return list(map(lambda x: x.clazz, labels))


def to_label(rs_object: ResultSheetObject, label_dict=None) -> Label:
    bb_box = rs_object.bounding_box
    # noinspection PyTypeChecker
    if label_dict:
        clazz = label_dict[rs_object.label]
    else:
        clazz = int(rs_object.label)

    return Label(clazz, (*bb_box[0], *bb_box[1]))


def list_to_labels(rs_objects: List[ResultSheetObject]) -> List[Label]:
    return list(map(to_label, rs_objects))


def dict_to_labels(rs_objects: Dict[object, List[ResultSheetObject]]) -> List[Label]:
    labels = []

    for _, value in rs_objects.items():
        labels.extend(list_to_labels(value))

    return labels


def get_info_labels(data: ResultSheet) -> InfoLabels:
    jmbag = []
    final_result = []
    first_table = []
    second_table = []

    def sort(y_and_labels: List[Tuple[int, List[Label]]]) -> List[List[Label]]:
        return list(map(lambda x: x[1], sorted(y_and_labels, key=lambda x: x[0])))

    for key, value in data.objects.items():
        labels = list_to_labels(value)

        if key.type == ResultSheetElementType.CREDENTIALS_TABLE_INFO:
            jmbag = labels
        elif key.type == ResultSheetElementType.FINAL_RESULT_ROW:
            final_result = labels
        elif key.start[0] < 400:
            first_table.append((key.start[1], labels))
        else:
            second_table.append((key.start[1], labels))

    results = sort(first_table) + sort(second_table)

    while len(results) < 20:
        results.append([])

    return InfoLabels(jmbag, results, final_result)

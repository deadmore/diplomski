import os
from typing import Any

import torchvision
from torch import load, save
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor


def faster_rcnn(class_count: int) -> Any:
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

    class_count = class_count + 1
    in_features = model.roi_heads.box_predictor.cls_score.in_features

    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, class_count)

    return model


def save_model(model: Any, path: str) -> None:
    dir_path = os.path.dirname(path)

    if not os.path.exists(dir_path):
        os.mkdir(dir_path)

    save(model, path)


def load_model(path: str) -> Any:
    if os.path.exists(path):
        return load(path)

    raise Exception("file {} doesnt exist".format(path))

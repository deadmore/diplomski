from random import randint

from PIL import ImageEnhance

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet


class MutateImageTransform(ResultSheetTransform):

    def __init__(self):
        self.contrast_bounds = (75, 125)
        self.sharpness_bounds = (0, 150)
        self.brightness_bounds = (75, 125)

    def __call__(self, target: ResultSheet) -> ResultSheet:
        rs_image = target.image

        contrast = randint(self.contrast_bounds[0], self.contrast_bounds[1]) / 100
        sharpness = randint(self.sharpness_bounds[0], self.sharpness_bounds[1]) / 100
        brightness = randint(self.brightness_bounds[0], self.brightness_bounds[1]) / 100

        image = ImageEnhance.Contrast(rs_image).enhance(contrast)
        image = ImageEnhance.Sharpness(image).enhance(sharpness)
        image = ImageEnhance.Brightness(image).enhance(brightness)

        target.image = image

        return target

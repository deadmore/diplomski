from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet, ResultSheetObject
from result_sheet.registry.dto import ResultSheetElementType, ResultSheetImageType
from result_sheet.registry.interfaces import ResultSheetElementRegistry
from util.tuple_util import tuple_sub


class JmbagRowTransform(ResultSheetTransform):

    def __init__(self, rs_element_registry: ResultSheetElementRegistry):
        self.rs_element_registry = rs_element_registry

    def __call__(self, result_sheet: ResultSheet):
        full_jmbag_row = self.rs_element_registry[ResultSheetElementType.FULL_CREDENTIALS_TABLE_INFO, 2]
        full_jmbag_row_start = tuple_sub(full_jmbag_row.start, result_sheet.offset)

        objects = {
            "full_jmbag_row": [ResultSheetObject("jmbag_row", start=full_jmbag_row_start, size=full_jmbag_row.size)],
        }

        return ResultSheet(result_sheet.rs_image, objects)


class ResultRowTransform(ResultSheetTransform):
    __iterable_dict = {
        ResultSheetImageType.FIRST_RESULT_TABLE: lambda: range(10),
        ResultSheetImageType.SECOND_RESULT_TABLE: lambda: range(10, 20),
    }

    def __init__(self, rs_element_registry: ResultSheetElementRegistry):
        self.rs_element_registry = rs_element_registry

    def __call__(self, result_sheet: ResultSheet):
        objects = []

        for index in ResultRowTransform.__iterable_dict[result_sheet.type]():
            full_result_row = self.rs_element_registry[ResultSheetElementType.FULL_RESULT_ROW, index]
            full_result_row_start = tuple_sub(full_result_row.start, result_sheet.offset)
            objects.append(ResultSheetObject("result_row", start=full_result_row_start, size=full_result_row.size))

        objects = {"full_result_rows": objects}

        return ResultSheet(result_sheet.rs_image, objects)

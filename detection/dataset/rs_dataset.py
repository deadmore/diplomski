from detection.dataset.base_dataset import BaseDataset
from result_sheet.registry.dto import ResultSheetImage, ResultSheetImageType


class ResultSheetDataset(BaseDataset):

    def _get_rs_image(self) -> ResultSheetImage:
        return self.rs_image_registry.get_rs_image(ResultSheetImageType.RESULT_SHEET)

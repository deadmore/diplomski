from typing import Any, Union

from result_sheet.generator.dto import ResultSheet


class ResultSheetTransform(object):

    def __call__(self, result_sheet: ResultSheet) -> Union[ResultSheet, Any]:
        pass

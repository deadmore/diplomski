from detection.dataset.base_dataset import BaseDataset
from result_sheet.crandom.funcs import random_int
from result_sheet.registry.dto import ResultSheetImage, ResultSheetImageType


class CredentialsTableDataset(BaseDataset):

    def _get_rs_image(self) -> ResultSheetImage:
        offset = random_int(20), random_int(15)

        return self.rs_image_registry.get_rs_image(ResultSheetImageType.CREDENTIALS_TABLE, offset=offset)

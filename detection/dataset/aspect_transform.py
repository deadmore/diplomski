from typing import Any, Union

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet
from result_sheet.registry.dto import ResultSheetImageType
from util.image_util import resize_width
from util.tuple_util import tuple_add


class AspectTransform(ResultSheetTransform):

    def __init__(self, width=665, color='white'):
        self.width = width
        self.color = color

    def __call__(self, result_sheet: ResultSheet) -> Union[ResultSheet, Any]:
        if result_sheet.type == ResultSheetImageType.JMBAG_ROW:
            return result_sheet

        image = result_sheet.image
        result, offset = resize_width(image)
        result_sheet.rs_image.image = result

        for objects in result_sheet.objects.values():
            for _object in objects:
                _object.start = tuple_add(_object.start, (offset, 0))

        return result_sheet

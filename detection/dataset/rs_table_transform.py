from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet, ResultSheetObject
from result_sheet.registry.dto import ResultSheetElementType
from result_sheet.registry.interfaces import ResultSheetElementRegistry


class ResultSheetTableTransform(ResultSheetTransform):

    def __init__(self, rs_element_registry: ResultSheetElementRegistry):
        self.rs_element_registry = rs_element_registry

    def __call__(self, target: ResultSheet) -> ResultSheet:
        credentials_table = self.rs_element_registry[ResultSheetElementType.TABLE, 0]
        ct_objects = [
            ResultSheetObject("credentials_table", start=credentials_table.start, size=credentials_table.size)]

        result_table_1 = self.rs_element_registry[ResultSheetElementType.TABLE, 1]
        rt1_objects = [
            ResultSheetObject("result_table", start=result_table_1.start, size=result_table_1.size)]

        result_table_2 = self.rs_element_registry[ResultSheetElementType.TABLE, 2]
        rt2_objects = [
            ResultSheetObject("result_table", start=result_table_2.start, size=result_table_2.size)]

        final_result_table = self.rs_element_registry[ResultSheetElementType.TABLE, 3]
        frt_objects = [
            ResultSheetObject("final_result_table", start=final_result_table.start, size=final_result_table.size)]

        objects = {
            "credentials_table": ct_objects,
            "result_table_1": rt1_objects,
            "result_table_2": rt2_objects,
            "final_result_table": frt_objects,
        }

        return ResultSheet(target.rs_image, objects)

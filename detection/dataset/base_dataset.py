from typing import Any, List

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.config.interfaces import ResultSheetConfigGenerator
from result_sheet.generator.dto import ResultSheet
from result_sheet.generator.interfaces import BaseFiller
from result_sheet.registry.dto import ResultSheetImage
from result_sheet.registry.interfaces import ResultSheetImageRegistry


class BaseDataset(object):

    def __init__(self, rs_image_registry: ResultSheetImageRegistry, rs_config_generator: ResultSheetConfigGenerator,
                 rs_filler: BaseFiller, epoch_size: int = 100000,
                 transforms: List[ResultSheetTransform] = ()):
        self.rs_image_registry = rs_image_registry
        self.rs_config_generator = rs_config_generator
        self.rs_filler = rs_filler
        self.epoch_size = epoch_size
        self.transforms = transforms.copy()

    def __getitem__(self, idx: int) -> Any:
        rs_image = self._get_rs_image()
        config = self.rs_config_generator.generate()

        result = ResultSheet(rs_image)
        self.rs_filler.fill(result, config)

        for transform in self.transforms:
            result = transform(result)

        return result

    def __len__(self) -> int:
        return self.epoch_size

    def _get_rs_image(self) -> ResultSheetImage:
        raise NotImplemented

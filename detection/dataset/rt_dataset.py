from detection.dataset.base_dataset import BaseDataset
from result_sheet.crandom.funcs import random_int, toss_coin
from result_sheet.registry.dto import ResultSheetImage, ResultSheetImageType


class ResultTableDataset(BaseDataset):

    def _get_rs_image(self) -> ResultSheetImage:
        _type = ResultSheetImageType.FIRST_RESULT_TABLE if toss_coin() else ResultSheetImageType.SECOND_RESULT_TABLE
        offset = random_int(15), random_int(10)

        return self.rs_image_registry.get_rs_image(_type, offset=offset)

from detection.dataset.base_dataset import BaseDataset
from result_sheet.crandom.funcs import random_int
from result_sheet.generator.default_ir_filler import AttributeKeys
from result_sheet.registry.dto import ResultSheetImage, ResultSheetImageType


class InfoRowDataset(BaseDataset):

    def _get_rs_image(self) -> ResultSheetImage:
        random_value = random_int(min=1, max=9)
        additional_attributes = {}

        if random_value < 4:
            offset = random_int(10), random_int(2)
            index = None
            _type = ResultSheetImageType.CREDENTIALS_TABLE
        elif random_value < 7:
            offset = random_int(5), random_int(2)
            index = random_int(19)
            _type = ResultSheetImageType.FIRST_RESULT_TABLE if index < 10 else ResultSheetImageType.SECOND_RESULT_TABLE
        else:
            offset = random_int(5), random_int(2)
            index = None
            _type = ResultSheetImageType.FINAL_RESULT_TABLE

        additional_attributes[AttributeKeys.OFFSET] = offset
        additional_attributes[AttributeKeys.INDEX] = index

        return self.rs_image_registry.get_rs_image(_type, offset=offset, additional_attributes=additional_attributes)

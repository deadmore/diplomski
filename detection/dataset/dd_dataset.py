from typing import Tuple

from torch import Tensor
from torchvision.transforms.functional import to_tensor

from result_sheet.registry.interfaces import CharacterImageRegistry


class DigitDetectionDataset(object):

    def __init__(self, ci_registry: CharacterImageRegistry):
        self.ci_registry = ci_registry.registry

    def __getitem__(self, idx: int) -> Tuple[Tensor, str]:
        label = chr(48 + idx % 10)
        index = idx // 10

        _object = self.ci_registry[label][index]
        image = to_tensor(_object.mask)

        return image, label

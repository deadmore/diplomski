from PIL.ImageDraw import ImageDraw

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet


class DrawBoundingBoxTransform(ResultSheetTransform):

    def __init__(self, color: str = "red", width: int = 3):
        self.color = color
        self.width = width

    def __call__(self, target: ResultSheet) -> ResultSheet:
        rs_image = target.image
        objects = target.objects

        for _, objects in objects.items():
            for _object in objects:
                min_x, min_y = _object.start
                width, height = _object.size
                points = [min_x, min_y, min_x + width, min_y + height]
                draw = ImageDraw(rs_image)
                draw.rectangle(points, outline=self.color, width=self.width)

        return target

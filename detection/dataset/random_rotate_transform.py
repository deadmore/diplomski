from math import cos, radians, sin
from random import uniform
from typing import List, Tuple

from PIL import Image
from PIL.Image import Image as PImage

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet


class RandomRotateTransform(ResultSheetTransform):

    def __init__(self, max_angle: float = 0, background: int = 255):
        self.max_angle = max_angle
        self.background = background

    @staticmethod
    def _rotate_image(image: PImage, angle: float, background: int = 255) -> PImage:
        im2 = image.convert('RGBA')
        rot = im2.rotate(angle, expand=1)
        fff = Image.new('RGBA', rot.size, (background,) * 4)
        out = Image.composite(rot, fff, rot)

        return out.convert(image.mode)

    @staticmethod
    def _rotate(position: Tuple[int, int], image_size: Tuple[int, int], shift: Tuple[int, int] = (0, 0),
                angle: float = 0) -> Tuple[int, int]:
        angle = radians(angle)

        px, py = position
        ox, oy = image_size[0] / 2, image_size[1] / 2

        x = ox + cos(angle) * (px - ox) + sin(angle) * (py - oy) - shift[0]
        y = oy - sin(angle) * (px - ox) + cos(angle) * (py - oy) - shift[1]

        return round(x), round(y)

    @staticmethod
    def _shift(size: Tuple[int, int], angle: float = 0) -> Tuple[int, int]:
        width, height = size[0], size[1]

        if angle > 0:
            top_left = RandomRotateTransform._rotate((0, 0), size, angle=angle)
            top_right = RandomRotateTransform._rotate((width, 0), size, angle=angle)
            return top_left[0], top_right[1]

        top_left = RandomRotateTransform._rotate((0, 0), size, angle=angle)
        bottom_left = RandomRotateTransform._rotate((0, height), size, angle=angle)

        return bottom_left[0], top_left[1]

    @staticmethod
    def _box_points(position: Tuple[int, int], size: Tuple[int, int], image_size: Tuple[int, int],
                    shift: Tuple[int, int], angle: float = 0):
        top_left = position
        top_right = top_left[0] + size[0], top_left[1]
        bottom_left = top_left[0], top_left[1] + size[1]
        bottom_right = top_left[0] + size[0], top_left[1] + size[1]

        if angle != 0:
            top_left = RandomRotateTransform._rotate(top_left, image_size, shift, angle)
            top_right = RandomRotateTransform._rotate(top_right, image_size, shift, angle)
            bottom_left = RandomRotateTransform._rotate(bottom_left, image_size, shift, angle)
            bottom_right = RandomRotateTransform._rotate(bottom_right, image_size, shift, angle)

        return [top_left, top_right, bottom_left, bottom_right]

    @staticmethod
    def _new_box(points: List[Tuple[int, int]], angle: float = 0) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        if angle == 0:
            result = (points[0], points[3])
        elif angle > 0:
            top_left = (points[0][0], points[1][1])
            bottom_right = (points[3][0], points[2][1])
            result = (top_left, bottom_right)
        else:
            top_left = (points[2][0], points[0][1])
            bottom_right = (points[1][0], points[3][1])
            result = (top_left, bottom_right)

        position = result[0]
        size = result[1][0] - result[0][0], result[1][1] - result[0][1]

        return position, size

    def __call__(self, target: ResultSheet) -> ResultSheet:
        angle = uniform(-self.max_angle, self.max_angle)
        background = self.background
        objects = target.objects
        rotated_image = RandomRotateTransform._rotate_image(target.image, angle, background)
        shift = RandomRotateTransform._shift(target.image.size, angle)

        for _, objects in objects.items():
            for _object in objects:
                box_points = RandomRotateTransform \
                    ._box_points(_object.start, _object.size, target.image.size, shift, angle)
                position, size = RandomRotateTransform._new_box(box_points, angle)
                _object.start = position
                _object.size = size

        target.image = rotated_image

        return target

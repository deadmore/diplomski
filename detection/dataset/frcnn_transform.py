from typing import Any, Dict

from torch import as_tensor, float32, int64
from torchvision.transforms.functional import to_tensor

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.generator.dto import ResultSheet


class FasterRCNNTransform(ResultSheetTransform):

    def __init__(self, label_dict: Dict[str, int]):
        if label_dict is None:
            raise Exception("label dict must be defined")

        self.label_dict = label_dict

    def __call__(self, target: ResultSheet) -> Any:
        boxes = []
        labels = []

        for _, objects in target.objects.items():
            for _object in objects:
                min_x, min_y = _object.start
                width, height = _object.size
                max_x, max_y = min_x + width, min_y + height
                label = self.label_dict[_object.label] + 1

                boxes.append([min_x, min_y, max_x, max_y])
                labels.append(label)

        image = to_tensor(target.image)
        target_dict = {
            "boxes": as_tensor(boxes, dtype=float32),
            "labels": as_tensor(labels, dtype=int64),
        }

        return image, target_dict

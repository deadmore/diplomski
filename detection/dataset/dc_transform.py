from typing import List, Tuple, Union

from PIL.Image import Image
from torch import Tensor
from torchvision.transforms.functional import to_tensor as f_to_tensor

from detection.dataset.interfaces import ResultSheetTransform
from result_sheet.crandom.funcs import random_int
from result_sheet.generator.dto import ResultSheet
from util.image_util import resize_and_invert_digit


class DigitDetectionTransform(ResultSheetTransform):

    def __init__(self, to_tensor=True):
        self.to_tensor = to_tensor

    def __call__(self, target: ResultSheet) -> List[Tuple[Union[Tensor, Image], int]]:
        rs_image = target.image
        objects = target.objects

        results = []

        for _, objects in objects.items():
            for _object in objects:
                min_x, min_y = _object.start
                width, height = _object.size

                possible_offset = (min(28 - width, 2), min(28 - height, 2))
                offset_x, offset_y = random_int(possible_offset[0]), random_int(possible_offset[1])

                points = [min_x - offset_x, min_y - offset_y, min_x + width + offset_x, min_y + height + offset_y]
                image = rs_image.crop(points)
                image = resize_and_invert_digit(image)

                if self.to_tensor:
                    image = f_to_tensor(image)

                result = (image, int(_object.label))
                results.append(result)

        return results

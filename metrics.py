from pipeline import run_eval_pipeline
from util.dataset_util import get_pipeline_dataset
from util.extraction_util import PredictionError
from util.label_util import get_info_labels, labels_to_classes


def metrics():
    result_sheet_hits = 0
    jmbag_hits = 0
    result_row_hits = 0
    too_few_jmbags = 0
    dataset = get_pipeline_dataset()

    for x in range(100):
        try:
            data = dataset[0]
            labels = get_info_labels(data)
            result = run_eval_pipeline(data.image)
            jmbag_hit = labels_to_classes(labels.jmbag_labels) == result.jmbag
            all_result_rows_hit = True

            for index in range(20):
                if labels_to_classes(labels.result_labels[index]) == result.results[index]:
                    result_row_hits += 1
                else:
                    all_result_rows_hit = False

                jmbag_hits += 1

            if jmbag_hit and all_result_rows_hit:
                result_sheet_hits += 1

        except PredictionError as e:
            too_few_jmbags += 1
            print(e.message)

        print('iter: {} result_sheet_hits: {}, jmbag_hits: {}, to_few_jmbag_digits: {},  result_row_hits: {}'
              .format(x, result_sheet_hits, jmbag_hits, too_few_jmbags, result_row_hits))


if __name__ == '__main__':
    metrics()

from util.dataset_util import get_test_rs_dataset

if __name__ == "__main__":
    dataset = get_test_rs_dataset()

    for i in range(20):
        img = dataset[i]
        img.save("./examples/{}.png".format(i))

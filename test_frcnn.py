import torch
from PIL.ImageDraw import ImageDraw
from matplotlib.patches import draw_bbox
from torchvision.transforms.functional import to_tensor

from util.dataset_util import get_test_ct_dataset
from util.model_registry import ct_model


def show_bbox(device, model, img):
    model.eval()
    with torch.no_grad():
        prediction = model([to_tensor(img).to(device)])
        boxes_and_scores = zip(prediction[0]['boxes'].tolist(), prediction[0]['scores'].tolist())

    img = img.convert("RGBA")
    for box, score in boxes_and_scores:
        if score > 0.7:
            x_min = round(box[0])
            y_min = round(box[1])
            x_max = round(box[2])
            y_max = round(box[3])

            draw = ImageDraw(img)
            draw.rectangle([x_min, y_min, x_max, y_max], outline="red", width=3)

    return img


if __name__ == "__main__":
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = ct_model()
    model.to(device)

    img = get_test_ct_dataset()[0]
    img = draw_bbox(device, model, img)
    img.show()
